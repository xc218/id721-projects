# IDS721 Week 1 Project


This is the Zola-generated website for my IDS721 projects.

*Home Page*
![Home Page](resources/home.png)

*About Page*
![About Page](resources/about.png)

*Project Page*
![Project Page](resources/project.png)
